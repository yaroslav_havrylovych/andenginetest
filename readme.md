AndEngine test project
======================

The small project aimed to find out how much entities [AndEngine](http://www.andengine.org/)
can be held on the screen simultaneously.

The test uses [Graphics](https://github.com/nicolasgramlich/AndEngine)
and [Physics](https://github.com/nicolasgramlich/AndEnginePhysicsBox2DExtension) extentions.

You can customize the test in a few ways (e.g. entities amount, does the object have children,
physics accuracy etc) and during the test you will have fps count and existing entities amount attached to 
[HUD](http://en.wikipedia.org/wiki/HUD_%28video_gaming%29).

Setting up
==========

The test uses gradle wrapper so it wouldn't be a problem to create an apk through the terminal
or using [AndroidStudio](http://developer.android.com/sdk/index.html).

Available customization
=======================

**Graphics**:

+ Objects amount - amount of objects that appear on the screen during the test.

+ Object entity type - it can be Entity or Sprite from png (file), or Sprite created out of
Bitmap (filled with particular color).

+ Entities arrangement - defines either entities are attached to Scene or to SpriteGroup.

+ Children - defines eithere object has a children or not; or children has to be emulated
(not attached as a children but as a separate sprite which is doing the same as children should).

**Physics:**

+ Velocity iterations - how much approximations has to calculate PhysicalWorld to resolve the velosity conflict.

+ Position iterations - how much approximations has to calculate PhysicalWorld to resolve the position conflict.

**Some notes:**

1. Reducing iterations (velocity and position) are really can help when a lot of entities are on the screen.

2. You can reach best results using SpriteGroup, small iterations value, 
without children and with sprites (out of the file or out of the code).

3. Primitives can't be added to SpriteGroup. So if you set SpriteGroup and Primitive,
then childreb will be added to Scene IGNORING SpriteGroup.

4. Zoom and scroll was added to see how it impacts on the performance (not for the convenience).


Possible crashes/bad things
===========================

1. I know that some devices will display black screen only if you try to zoom or to scroll
(or just touch the screen). It's somewhere in the engine (or in my hands).
I will probably take a look on this later and maybe will disable this operations for a bulk of devices.
2. If you choose Sprite (code) option and then stop Activity during the test then, after
resuming the test, the app will crash because of bitmap already recycled. It was recycled in the
BitmapTextureAtals I think. 
Same I've found [here](http://www.andengine.org/forums/gles1/error-can-t-call-getpixels-on-a-recycled-bitmap-t2879.html).
I think it's possible to have some work around (e.g. sprite out of flie works, so we can still the solution from there),
but this is not in the scope of the current test.
