package com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.Toast;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.R;
import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.config.Children;
import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.config.EntityArrangement;
import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.config.EntityType;
import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.config.Keys;
import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.helpers.EngineHelper;
import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.helpers.PopulatedObject;
import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.touch.SceneTouchListener;

import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.IEntity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.batch.SpriteGroup;
import org.andengine.entity.text.Text;
import org.andengine.entity.util.FPSLogger;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.font.IFont;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.adt.color.Color;

import java.util.Random;


/** Here is the action of the test. All thing will happen here */
public class TestMainActivity extends BaseGameActivity {
    /** screen height */
    private static final int sScreenHeight = 1080;
    /** screen width */
    private static final int sScreenWidth = 1920;
    /** hud used to display fps and units amount (always on the screen) */
    private HUD mHud;
    /** each created object physical characteristics */
    final FixtureDef objectFixtureDef = PhysicsFactory.createFixtureDef(1, 0.5f, 0.5f);
    /** all physical bounds, rules are because of this thing */
    private PhysicsWorld mPhysicsWorld;
    /** entities/sprites will be attached to this scene */
    private Scene mScene;
    /** FPS value on the screen */
    private Text mFpsText;
    /** Objects amount value on the screen */
    private Text mObjectsText;
    /** font to display text on the screen */
    private IFont mFont;
    /** maximal objects amount */
    private int mObjectsAmount;
    /** used to create object for populate the {@link Scene} or {@link SpriteGroup} */
    PopulatedObject mPopulatedObject;
    /** sprite group used to attach sprites (if test has this in setup) */
    private SpriteGroup mSpriteGroup;
    /** decides should entity be place on a {@link Scene} or {@link SpriteGroup} */
    EntityArrangement mEntitiesArrangement;
    /**
     * primitive or a {@link org.andengine.entity.sprite.Sprite}
     * (sprite can be code created or loaded from the file
     */
    private EntityType mEntityType;
    /** should object contains children or not */
    private Children mChildren;
    /** test camera */
    private SmoothCamera mCamera;
    /* physic world params */
    private int mVelocityIterations = PhysicsWorld.VELOCITY_ITERATIONS_DEFAULT;
    private int mPositionIterations = PhysicsWorld.POSITION_ITERATIONS_DEFAULT;

    @Override
    protected void onCreate(Bundle pSavedInstanceState) {
        super.onCreate(pSavedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            throw new IllegalStateException("Trying to start the test without init params");
        }
        mObjectsAmount = bundle.getInt(Keys.OBJECTS_AMOUNT);
        mEntitiesArrangement = (EntityArrangement)
                bundle.getSerializable(Keys.ENTITIES_ARRANGEMENT);
        mEntityType = (EntityType) bundle.getSerializable(Keys.ENTITY_TYPE);
        mChildren = (Children) bundle.getSerializable(Keys.CHILDREN);
        mVelocityIterations = bundle.getInt(Keys.VELOCITY_ITERATIONS);
        mPositionIterations = bundle.getInt(Keys.POSITION_ITERATIONS);

        mPopulatedObject = new PopulatedObject(mEntityType, mChildren);
    }

    @Override
    public EngineOptions onCreateEngineOptions() {
        // init camera
        mCamera = new SmoothCamera(0, 0, sScreenWidth, sScreenHeight,
                sScreenWidth, sScreenHeight, SceneTouchListener.MAX_ZOOM_FACTOR);
        mCamera.setBounds(0, 0, sScreenWidth, sScreenHeight);
        mCamera.setBoundsEnabled(true);

        //init hud
        mHud = new HUD();
        mCamera.setHUD(mHud);

        EngineOptions engineOptions = new EngineOptions(
                true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(
                sScreenWidth, sScreenHeight), mCamera);

        mPhysicsWorld = new PhysicsWorld(new Vector2(0, 0), false,
                mVelocityIterations, mPositionIterations);

        return engineOptions;
    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) {
        //load font
        mFont = FontFactory.create(getFontManager(), getTextureManager(), 256, 256,
                Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 45,
                Color.WHITE.hashCode());
        mFont.load();

        //load objects
        BitmapTextureAtlas textureAtlas =
                mPopulatedObject.loadResources(getTextureManager(), this);
        if (mEntitiesArrangement == EntityArrangement.SPRITE_GROUP) {
            //we can't add primitives to SpriteGroupm
            if (mEntityType != EntityType.PRIMITIVE) {
                mSpriteGroup = new SpriteGroup(textureAtlas,
                        mChildren != Children.NO_CHILDREN ? mObjectsAmount * 2 : mObjectsAmount,
                        getVertexBufferObjectManager());
                mSpriteGroup.setChildrenVisible(mChildren == Children.NORMAL_CHILDREN);
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast
                                .makeText(TestMainActivity.this,
                                        R.string.primitives_in_sprite_groups, Toast.LENGTH_LONG)
                                .show();
                    }
                });
            }
        }
        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }

    @Override
    public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) {
        mScene = new Scene();
        mScene.registerUpdateHandler(mPhysicsWorld);
        if (mSpriteGroup != null) {
            mScene.attachChild(mSpriteGroup);
        }
        mScene.setOnSceneTouchListener(new SceneTouchListener(mCamera));
        pOnCreateSceneCallback.onCreateSceneFinished(mScene);
    }

    @Override
    public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) {
        //bounds
        EngineHelper.createBounds(mPhysicsWorld, sScreenWidth, sScreenHeight);

        //hud
        mFpsText = new Text(200, sScreenHeight - 50, mFont, "fps: 60.00", 20,
                getVertexBufferObjectManager());
        mFpsText.setColor(Color.GREEN);
        mHud.attachChild(mFpsText);
        mObjectsText = new Text(sScreenWidth - 200, sScreenHeight - 50, mFont, "objects: 60.00",
                20, getVertexBufferObjectManager());
        mObjectsText.setColor(Color.BLUE);
        mHud.attachChild(mObjectsText);

        //populate scene
        mScene.registerUpdateHandler(new TimerHandler(1, true,
                new Populator(mEntityType.isSprite()
                        && mEntitiesArrangement == EntityArrangement.SPRITE_GROUP
                        ? mSpriteGroup : mScene)));

        //display fps
        mEngine.registerUpdateHandler(new FPSLogger() {
            @Override
            protected void onLogFPS() {
                mFpsText.setText(String.format("fps: %.2f", this.mFrames / this.mSecondsElapsed));
            }
        });

        pOnPopulateSceneCallback.onPopulateSceneFinished();
    }

    /**
     * iteratively populate scene (or sprite batch) with objects
     * until reach {@link TestMainActivity#mObjectsAmount}
     */
    private class Populator implements ITimerCallback {
        /** objects amount at current step */
        private int mCurrentObjectsAmount = 0;
        /** how much objects will be added at each step */
        private int mObjectIncrease = 5;
        /** object size */
        private int mObjectSize = 36;
        /** entity to attach objects (children) to */
        IEntity mParentEntity;

        public Populator(IEntity parentEntity) {
            mParentEntity = parentEntity;
        }

        @Override
        public void onTimePassed(TimerHandler pTimerHandler) {
            mCurrentObjectsAmount += mObjectIncrease;
            if (mCurrentObjectsAmount > mObjectsAmount) {
                mEngine.unregisterUpdateHandler(pTimerHandler);
                return;
            }
            int x = mObjectSize + new Random().nextInt(sScreenWidth - 2 * mObjectSize);
            int y = mObjectSize + new Random().nextInt(sScreenHeight - 2 * mObjectSize);
            for (int i = 0; i < mObjectIncrease; i++) {
                //object creation
                IEntity[] entities = mPopulatedObject.createEntity(x, y, getVertexBufferObjectManager());
                final IEntity object = entities[0];
                final Body body = PhysicsFactory.createBoxBody(
                        mPhysicsWorld, object, BodyDef.BodyType.DynamicBody, objectFixtureDef);
                mPhysicsWorld.registerPhysicsConnector(
                        new PhysicsConnector(object, body, true, false));
                for (IEntity entity : entities) {
                    mParentEntity.attachChild(entity);
                }

                //object moving
                EngineHelper.moveToPoint(body, object, new Random().nextInt(1920), new Random().nextInt(1080));
                object.registerUpdateHandler(new TimerHandler(.5f, true, new ITimerCallback() {
                    private volatile long time = System.currentTimeMillis();

                    @Override
                    public void onTimePassed(TimerHandler pTimerHandler) {
                        if (System.currentTimeMillis() - time >
                                (7 + new Random().nextInt(11)) * 1000) {
                            int x = new Random().nextInt(sScreenWidth);
                            int y = new Random().nextInt(sScreenHeight);
                            EngineHelper.moveToPoint(body, object, x, y);
                            time = System.currentTimeMillis();
                        }
                    }
                }));
            }
            mObjectsText.setText("objects: " + mCurrentObjectsAmount);
        }
    }
}
