package com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.helpers;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import org.andengine.entity.IEntity;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.util.math.MathConstants;
import org.andengine.util.math.MathUtils;

/** some methods moved here to clear activities */
public class EngineHelper {
    private EngineHelper() {
    }

    /** @return angle in rad between entity will point to x,y */
    public static float calculateRotation(IEntity entity, float x, float y) {
        // next till the end will calculate angle
        float currentX = entity.getX(),
                currentY = entity.getY();

        return calculateRotation(currentX, currentY, x, y);
    }

    /** @return angle in rad between startX, startY will point to x,y */
    public static float calculateRotation(float startX, float startY, float x, float y) {
        float a = Math.abs(startX - x),
                b = Math.abs(startY - y);

        float newAngle = (float) Math.atan(b / a);

        if (startY > y) {
            if (startX > x) return 3 * MathConstants.PI / 2 - newAngle;
            else return newAngle + MathConstants.PI / 2;
        }

        if (startX > x) return newAngle + 3 * MathConstants.PI / 2;
        return MathConstants.PI / 2 - newAngle;
    }

    /** Set direction to body out of current position to [x,y]. Entity used to set angle. */
    public static void moveToPoint(final Body body, final IEntity entity, final float x, final float y) {
        entity.setRotation(MathUtils.radToDeg(calculateRotation(entity, x, y)));

        float distanceX = x - entity.getX(),
                distanceY = y - entity.getY();
        float absDistanceX = Math.abs(distanceX),
                absDistanceY = Math.abs(distanceY),
                maxAbsDistance = absDistanceX > absDistanceY ? absDistanceX : absDistanceY;
        float ordinateSpeed = 1.2f * distanceX / maxAbsDistance,
                abscissaSpeed = 1.2f * distanceY / maxAbsDistance;

        body.setLinearVelocity(ordinateSpeed, abscissaSpeed);
    }

    /**
     * creates invisible static walls (bound the screen)
     * so objects will not move out of the screen
     */
    public static void createBounds(PhysicsWorld physicsWorld, int width, int height) {
        FixtureDef objectFixtureDef = PhysicsFactory.createFixtureDef(1, 1f, .5f);
        PhysicsFactory.createLineBody(
                physicsWorld,
                -1, -1,
                -1, height + 1,
                objectFixtureDef);
        PhysicsFactory.createLineBody(
                physicsWorld,
                -1, -1,
                width + 1, -1,
                objectFixtureDef);
        PhysicsFactory.createLineBody(
                physicsWorld,
                width + 1, -1,
                width + 1, height + 1,
                objectFixtureDef);
        PhysicsFactory.createLineBody(physicsWorld,
                width + 1, height + 1,
                -1, height + 1,
                objectFixtureDef);
    }
}
