package com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.config;


/** entity type */
public enum EntityType {
    /** primitive entity */
    PRIMITIVE,
    /** sprite created out of a bitmap which we create programmatically */
    CODE_SPRITE,
    /** sprite loaded out of file */
    FILE_SPRITE;

    /**
     * decides is current {@link EntityType} has type  of sprite or primitive
     *
     * @return true if current object is sprite and false in the other case
     */
    public boolean isSprite() {
        return this == CODE_SPRITE || this == FILE_SPRITE;
    }

    public static EntityType getValueOf(int id) {
        if (PRIMITIVE.ordinal() == id) {
            return PRIMITIVE;
        } else if (CODE_SPRITE.ordinal() == id) {
            return CODE_SPRITE;
        } else {
            return FILE_SPRITE;
        }
    }
}
