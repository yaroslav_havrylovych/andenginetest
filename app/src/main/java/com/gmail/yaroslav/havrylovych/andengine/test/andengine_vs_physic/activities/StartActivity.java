package com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.R;
import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.config.Children;
import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.config.EntityArrangement;
import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.config.EntityType;
import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.config.Keys;

import org.andengine.extension.physics.box2d.PhysicsWorld;

/** startup activity vs test customizations */
public class StartActivity extends Activity {
    private Spinner mEntitiesAmount;
    private Spinner mEntityType;
    private Spinner mEntityArrangement;
    private Spinner mChildren;
    private Spinner mVelocityItearations;
    private Spinner mPosiitionIterations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tetst_customization_layout);
        //find by id
        mEntitiesAmount = (Spinner) findViewById(R.id.entities_amount);
        mEntityType = (Spinner) findViewById(R.id.entity_type);
        mEntityArrangement = (Spinner) findViewById(R.id.entities_arrangement);
        mChildren = (Spinner) findViewById(R.id.children);
        mVelocityItearations = (Spinner) findViewById(R.id.velocity_iterations);
        mPosiitionIterations = (Spinner) findViewById(R.id.position_iterations);
        Button startTest = (Button) findViewById(R.id.start_the_test);
        //init
        startTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TestMainActivity.class);

                //amount
                int intValue;
                try {
                    intValue = Integer.parseInt(mEntitiesAmount.getSelectedItem().toString());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    intValue = 350;
                }
                intent.putExtra(Keys.OBJECTS_AMOUNT, intValue);

                //type
                EntityType entityType = EntityType.getValueOf(
                        (int) mEntityType.getSelectedItemId());
                intent.putExtra(Keys.ENTITY_TYPE, entityType);

                //arrangement
                EntityArrangement entityArrangement = EntityArrangement.getValueOf(
                        (int) mEntityArrangement.getSelectedItemId());
                intent.putExtra(Keys.ENTITIES_ARRANGEMENT, entityArrangement);

                //children
                Children children = Children.getValueOf(
                        (int) mChildren.getSelectedItemId());
                intent.putExtra(Keys.CHILDREN, children);

                //velocity iterations
                try {
                    intValue = Integer.parseInt(mVelocityItearations.getSelectedItem().toString());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    intValue = PhysicsWorld.VELOCITY_ITERATIONS_DEFAULT;
                }
                intent.putExtra(Keys.VELOCITY_ITERATIONS, intValue);

                //position iterations
                try {
                    intValue = Integer.parseInt(mPosiitionIterations.getSelectedItem().toString());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    intValue = PhysicsWorld.POSITION_ITERATIONS_DEFAULT;
                }
                intent.putExtra(Keys.POSITION_ITERATIONS, intValue);

                startActivity(intent);
            }
        });
    }
}