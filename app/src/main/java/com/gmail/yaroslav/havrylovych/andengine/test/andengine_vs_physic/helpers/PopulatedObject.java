package com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;

import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.config.Children;
import com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.config.EntityType;

import org.andengine.entity.IEntity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.source.EmptyBitmapTextureAtlasSource;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Unload activity by handle some logic concerning entities which will populate the screen.
 * <br/>
 * 1. Contains resource loading.
 * <br/>
 * 2. Entities creation logic (i.e. vs or vs-out children, sprites or primitives).
 */
public class PopulatedObject {
    private final int mObjectSize = 16;
    private final int mChildWidth = 16;
    private final int mChildHeight = 3;
    /** children color */
    private final static int mChildrenColor = Color.GREEN;
    /** object color */
    private final static org.andengine.util.adt.color.Color mObjectArgbColor
            = org.andengine.util.adt.color.Color.RED;
    /** object texture region (only if sprites used) */
    private ITextureRegion mObjectTextureRegion;
    /** children texture region (only if sprites used) */
    private ITextureRegion mChildrenTextureRegion;
    /** created children (if needed) type */
    private final Children mChildren;
    /** created entity type */
    private final EntityType mEntityType;

    public PopulatedObject(EntityType entityType, Children children) {
        mEntityType = entityType;
        mChildren = children;
    }

    public IEntity[] createEntity(int x, int y, VertexBufferObjectManager objectManager) {
        boolean separateChildren = mChildren == Children.SEPARATED_CHILDREN;
        IEntity[] ret = new IEntity[separateChildren ? 2 : 1];
        IEntity object;
        IEntity child = null;
        if (children()) {
            child = createEntity(0, 0, mChildWidth, mChildHeight,
                    mChildrenTextureRegion, objectManager, null);
            child.setColor(mChildrenColor);
            if (separateChildren) {
                ret[1] = child;
            }
        }

        object = createEntity(x, y, mObjectSize, mObjectSize,
                mObjectTextureRegion, objectManager,
                separateChildren ? child : null);
        if (mEntityType == EntityType.PRIMITIVE) {
            object.setColor(mObjectArgbColor);
        }
        if (mChildren == Children.NORMAL_CHILDREN && child != null) {
            //set child position atop of object
            child.setPosition(object.getWidth() / 2, object.getHeight() + child.getHeight());
            object.attachChild(child);
        }
        ret[0] = object;
        return ret;
    }

    private IEntity createEntity(float x, float y, int width, int height,
                                 ITextureRegion textureRegion,
                                 VertexBufferObjectManager objectManager,
                                 final IEntity child) {
        //sprites only
        if (mEntityType.isSprite()) {
            if (child != null) {
                return new Sprite(x, y, textureRegion, objectManager) {
                    @Override
                    public void setPosition(float pX, float pY) {
                        super.setPosition(pX, pY);
                        child.setPosition(pX - getWidth() / 2,
                                pY + getHeight() / 2 + child.getHeight());
                    }
                };
            }
            return new Sprite(x, y, textureRegion, objectManager);
        }
        //primitive
        if (child != null) {
            return new Rectangle(x, y, width, height, objectManager) {
                @Override
                public void setPosition(float pX, float pY) {
                    super.setPosition(pX, pY);
                    child.setPosition(pX, pY + getHeight() / 2 + child.getHeight());
                }
            };
        }
        return new Rectangle(x, y, width, height, objectManager);
    }

    /** load sprites from files or creating bitmaps */
    public BitmapTextureAtlas loadResources(TextureManager textureManager, Context context) {
        if (!mEntityType.isSprite()) {
            return null;
        }
        BitmapTextureAtlas textureAtlas = new BitmapTextureAtlas(textureManager, 128, 128);
        if (mEntityType == EntityType.FILE_SPRITE) {
            //object
            mObjectTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(textureAtlas, context,
                    "object.png", 0, 0);
            //child
            if (children()) {
                mChildrenTextureRegion =
                        BitmapTextureAtlasTextureRegionFactory.createFromAsset(textureAtlas, context,
                                "child.png", 0, mObjectSize + 5);
            }
        } else {
            //object
            Bitmap bitmap = Bitmap.createBitmap(mObjectSize, mObjectSize, Bitmap.Config.ARGB_8888);
            bitmap.eraseColor(Color.RED);
            EmptyBitmapTextureAtlasSource textureAtlasSource = new BitmapTextureAtlasSource(bitmap);
            mObjectTextureRegion =
                    BitmapTextureAtlasTextureRegionFactory.createFromSource
                            (textureAtlas, textureAtlasSource, 0, 0);
            //child
            if (children()) {
                bitmap = Bitmap.createBitmap(mChildWidth, mChildHeight, Bitmap.Config.ARGB_8888);
                bitmap.eraseColor(mChildrenColor);
                textureAtlasSource = new BitmapTextureAtlasSource(bitmap);
                mChildrenTextureRegion =
                        BitmapTextureAtlasTextureRegionFactory.createFromSource
                                (textureAtlas, textureAtlasSource, 0, mObjectSize + 5);
            }
        }
        textureAtlas.load();
        return textureAtlas;
    }

    private boolean children() {
        return mChildren == Children.NORMAL_CHILDREN || mChildren == Children.SEPARATED_CHILDREN;
    }

    /** create texture atlas source out of bitmap */
    private class BitmapTextureAtlasSource extends EmptyBitmapTextureAtlasSource {
        private Bitmap mBitmap;

        public BitmapTextureAtlasSource(Bitmap bitmap) {
            super(bitmap.getWidth(), bitmap.getHeight());
            mBitmap = bitmap;
        }

        @Override
        public Bitmap onLoadBitmap(Bitmap.Config pBitmapConfig, boolean pMutable) {
            return mBitmap;
        }
    }
}
