package com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.config;

/** entities arrangement place (i.e. scene or sprite group) */
public enum EntityArrangement {
    SPRITE_GROUP,
    SCENE;

    public static EntityArrangement getValueOf(int id) {
        if (SPRITE_GROUP.ordinal() == id) {
            return SPRITE_GROUP;
        } else {
            return SCENE;
        }
    }
}
