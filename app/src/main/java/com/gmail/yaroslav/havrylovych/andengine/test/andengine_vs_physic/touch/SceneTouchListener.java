package com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.touch;

import org.andengine.engine.camera.SmoothCamera;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.detector.PinchZoomDetector;
import org.andengine.input.touch.detector.ScrollDetector;
import org.andengine.input.touch.detector.SurfaceScrollDetector;

/** Test scene touch listener.Handle camera zoom and scroll capabilities */
public class SceneTouchListener implements
        IOnSceneTouchListener,
        PinchZoomDetector.IPinchZoomDetectorListener,
        ScrollDetector.IScrollDetectorListener {
    public static final float MAX_ZOOM_FACTOR = 5;
    public static final float MIN_ZOOM_FACTOR = 1;
    private PinchZoomDetector mZoomDetector;
    private ScrollDetector mScrollDetector;
    private SmoothCamera mCamera;
    private float mInitialTouchZoomFactor;

    public SceneTouchListener(SmoothCamera camera) {
        mZoomDetector = new PinchZoomDetector(this);
        mScrollDetector = new SurfaceScrollDetector(this);
        mCamera = camera;
    }

    @Override
    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        mZoomDetector.onSceneTouchEvent(pScene, pSceneTouchEvent);
        if (mZoomDetector.isZooming()) {
            mScrollDetector.setEnabled(false);
        } else {
            if (pSceneTouchEvent.isActionDown()) {
                mScrollDetector.setEnabled(true);
            }
            mScrollDetector.onTouchEvent(pSceneTouchEvent);
        }
        return true;
    }

    @Override
    public void onPinchZoomStarted(PinchZoomDetector pPinchZoomDetector, TouchEvent pSceneTouchEvent) {
        mInitialTouchZoomFactor = mCamera.getZoomFactor();
    }

    @Override
    public void onPinchZoom(PinchZoomDetector pPinchZoomDetector, TouchEvent pTouchEvent, float pZoomFactor) {
        final float newZoomFactor = mInitialTouchZoomFactor * pZoomFactor;
        if (newZoomFactor < MAX_ZOOM_FACTOR && newZoomFactor > MIN_ZOOM_FACTOR) {
            mCamera.setZoomFactor(newZoomFactor);
        }
    }

    @Override
    public void onPinchZoomFinished(PinchZoomDetector pPinchZoomDetector, TouchEvent pTouchEvent, float pZoomFactor) {
        //unused
    }

    @Override
    public void onScrollStarted(ScrollDetector pScollDetector, int pPointerID, float pDistanceX, float pDistanceY) {
        //unused
    }

    @Override
    public void onScroll(ScrollDetector pScollDetector, int pPointerID, float pDistanceX, float pDistanceY) {
        final float zoomFactor = mCamera.getZoomFactor();
        mCamera.offsetCenter(-pDistanceX / zoomFactor, pDistanceY / zoomFactor);
    }

    @Override
    public void onScrollFinished(ScrollDetector pScollDetector, int pPointerID, float pDistanceX, float pDistanceY) {
        //unused
    }
}
