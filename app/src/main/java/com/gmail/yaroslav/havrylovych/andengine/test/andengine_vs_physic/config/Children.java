package com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.config;

/**
 * does the tested object will have children
 * (and children which can be emulated i.e. entities which will follow the object)
 */
public enum Children {
    /** object will not have children */
    NO_CHILDREN,
    /** object will have a child atop of it */
    NORMAL_CHILDREN,
    /**
     * object will not have a child but will have an entity which will follow the object
     * and will be separated in the place where the child should be
     */
    SEPARATED_CHILDREN;

    public static Children getValueOf(int id) {
        if (NO_CHILDREN.ordinal() == id) {
            return NO_CHILDREN;
        } else if (NORMAL_CHILDREN.ordinal() == id) {
            return NORMAL_CHILDREN;
        } else {
            return SEPARATED_CHILDREN;
        }
    }
}
