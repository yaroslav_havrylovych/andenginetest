package com.gmail.yaroslav.havrylovych.andengine.test.andengine_vs_physic.config;

/** String keys/constants used into the project */
public interface Keys {
    /* graphics */
    String OBJECTS_AMOUNT = "objects_amount";
    String ENTITY_TYPE = "entity_type";
    String CHILDREN = "children";
    String ENTITIES_ARRANGEMENT = "entity_arrangement";
    
    /* physics */
    String POSITION_ITERATIONS = "position_iterations";
    String VELOCITY_ITERATIONS = "velocity_iterations";
}
